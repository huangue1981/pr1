import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';
import { DestinoApiClient } from '../models/destinos-api-client.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;

  // destinos: DestinoViaje[];
  constructor(private destinoApiClient: DestinoApiClient) {
    // this.destinos = [];
    this.onItemAdded = new EventEmitter();
  }

  ngOnInit() {}

  guardar(nombre: string, url: string): boolean {
    this.destinos.push(new DestinoViaje(nombre, url));
    this.destinosApiClient.add(d);
    return false;
  }

  agregado(d: DestinoViaje) {
    // this.destinos.push(new DestinoViaje(nombre, url));
    this.destinoApiClient.add(d);
    this.onItemAdded.emit(d);
  }

  elegido(d: DestinoViaje) {
    this.destinos.forEach(function (x) {
      x.setSelected(false);
      this.destinosApiClient.getAll().forEach(x => x.setSelected(false));
    });
    d.setSelected(true);
  }
}
